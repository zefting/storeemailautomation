# The following code is based on an example given here: https://realpython.com/python-send-email/#make-a-csv-file-with-relevant-personal-info
# This example uses a .csv file as a database, but it should connect to a database and pull the emails from there

#Imports
import csv, smtplib, ssl
import mysql.connector
from mysql.connector import Error

try:
    connection = mysql.connector.connect(host='localhost',
                                         database='store',
                                         user='DBuser',
                                         password='DBPassword')

    sql_select_Query = "select * from users"
    cursor = connection.cursor()
    cursor.execute(sql_select_Query)
    records = cursor.fetchall()
    # Email Begin

    # Set from Email Adress
    from_address = "x@StoreEmail.com"
    # Email password
    password = "Password"

    # For loop generating dynamic context
    for row in records:
        message = """ Hello row[1] row[2], Thank you for your order with the order ID: row[0] """
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.StoreEmailServer.com", 465, context=context) as server:
            server.login(from_address, password)
            server.sendmail(
                from_address,
                row[3]
            )

except Error as e:
    print("Error reading data from MySQL table", e)
finally:
    if (connection.is_connected()):
        connection.close()
        cursor.close()
        print("MySQL connection is closed")
