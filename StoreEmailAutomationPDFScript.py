# The following code is based on an example given here: https://realpython.com/python-send-email/#make-a-csv-file-with-relevant-personal-info

import email, smtplib, ssl

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

subject = "Order Invoice"
sender_email = "Store@storeEmailAdress.com" # this is the stores email adress.
receiver_email = "your@gmail.com" # This variable is will be populated with data from the order process 
password = "Password"



# This part of the script generates a multipart message and futhermore sets the headers used for the email.
message = MIMEMultipart("alternative")
message["From"] = sender_email
message["To"] = receiver_email
message["Subject"] = subject

# It is best practice to send both a plain and a html version of the email. 
# The "plaintext" veriable contains the plan text version of the email and the html var contains the html code. 

# Create the plain-text and HTML version of your message
plaintext = """\
Thank you for shopping with us!
Attached is your order invoice. 
Have a great day!
"""

htmlcode = """\
<html>
  <body>
    <p>Thank you for shopping with us!<br>
       Attached is your order invoice. <br>
        Have a great day!
    </p>
  </body>
</html>
"""
# More can be done with html, but for the sake of brevity I have excluded this.

# This part of the code will generate MIMEText objects for the above veriables
text = MIMEText(plaintext, "plain")
html = MIMEText(htmlcode, "html")

# Add HTML/plain-text parts to MIMEMultipart message
# The email client will try to render the last part first
message.attach(text)
message.attach(html)

filename = "invoice/invoice.pdf"  # For this example the invoice file will be generated on order completion, and is located in the invoice directory

# Open PDF file in binary mode
with open(filename, "rb") as attachment:
    # Add file as application/octet-stream
    # Email client can usually download this automatically as attachment
    part = MIMEBase("application", "octet-stream")
    part.set_payload(attachment.read())

# Encode file in ASCII characters to send by email    
encoders.encode_base64(part)

# Add header as key/value pair to attachment part
part.add_header(
    "Content-Disposition",
    f"attachment; filename= {filename}",
)

# Add attachment to message and convert message to string
message.attach(part)
text = message.as_string()

# Log in to server using secure context and send email
context = ssl.create_default_context()
with smtplib.SMTP_SSL("smtp.storeServer.com", 465, context=context) as server:
    server.login(sender_email, password)
    server.sendmail(sender_email, receiver_email, text)